﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildShuffler : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        ShuffleChildren();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShuffleChildren()
    {
        List<GameObject> temp = new List<GameObject>();
        foreach (Transform child in transform)
        {
            temp.Add(child.gameObject);
        }
        temp.ForEach(o => o.transform.SetParent(null, false));
        while (temp.Count > 0)
        {
            int randomIndex = Random.Range(0, temp.Count);
            temp[randomIndex].transform.SetParent(transform, false);
            temp.RemoveAt(randomIndex);
            temp.TrimExcess();
        }

    }
    public void ActivateSpawn()
    {

        SpeciesCard[] i = GetComponentsInChildren<SpeciesCard>();

        StartCoroutine(spawn(i));
    }
    public IEnumerator spawn(SpeciesCard[] IS)
    {

        for (int i = 0; i < IS.Length; i++)
        {

            IS[i].SendMessage("Spawn");

            yield return new WaitForSeconds(0.2f);

        }
    }
}
