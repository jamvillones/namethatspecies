﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_SetTextViaResult : MonoBehaviour
{
    public Text[] TextComponent;
    [SerializeField]
    Image img;
    [SerializeField]
    Sprite questionMark;

    // Use this for initialization
    void Start()
    {
       // img = GetComponentInChildren<Image>();
        TextComponent = GetComponentsInChildren<Text>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnEnable()
    {
        this.AddEventListenerGlobal<Event_SelectResult>(Event_SelectResult_CB);
    }
    private void OnDisable()
    {
        this.RemoveEventListenerGlobal<Event_SelectResult>(Event_SelectResult_CB);
    }
    void Event_SelectResult_CB(Event_SelectResult e)
    {
        if (e.Correct)
        {
            TextComponent[0].text = e.animalinfo.Name;
            TextComponent[1].text = e.animalinfo.ScientificName;
            img.sprite = Sprite.Create(e.animalinfo.Image, new Rect(0.0f, 0.0f, e.animalinfo.Image.width, e.animalinfo.Image.height), new Vector2(0.5f, 0.5f));
        }
        else
        {
            TextComponent[0].text = "Creatures do not match!";
            TextComponent[1].text = "Try another one.";
            img.sprite = questionMark;
        }
    }
}
