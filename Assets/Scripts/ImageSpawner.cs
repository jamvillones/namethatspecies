﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageSpawner : MonoBehaviour {
    public float Ypoint;
    public Transform ImageToBeSpawned;
    public Transform t;
    public float Speed = 5;
    public Texture2D texture { get { return GetComponent<SpeciesCard>().species.Image; } }
  

	// Use this for initialization
	void Start () {
        Destroy(this, 5);
	}
	
	// Update is called once per frame
	void Update () {
        if (t == null)
            return;
        // t.position = Vector3.MoveTowards(t.position,this.transform.position,Speed*Time.deltaTime);
        t.position = Vector3.Lerp(t.position, transform.position, Speed * Time.deltaTime);
        if (Vector2.Distance(t.position, transform.position) <= 0)
        {
            this.enabled = false;
        }
           
		
	}
    public void Spawn()
    {
        if (ImageToBeSpawned == null || Ypoint == 0)
        {
            Debug.Log("please assign the image to be spawned or make sure the spawn point is not (0,0)");
            return;
        }
        t = Instantiate(ImageToBeSpawned, new Vector3(transform.position.x, Ypoint, transform.position.z), Quaternion.identity);
        UnityEngine.UI.Image img = t.GetComponent<UnityEngine.UI.Image>();
        img.sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        img.preserveAspect = true;
        // t.GetComponent<UnityEngine.UI.Image>().sprite = Sprite.Create(texture, new Rect(0.0f,0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        t.SetParent(this.transform);
        t.localScale = this.transform.localScale;

    }
}
