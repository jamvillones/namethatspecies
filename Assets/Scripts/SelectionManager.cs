﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SelectionManager : Core.Utilities.Singleton<SelectionManager>
{
    public GameObject LineConnector;
    public GameObject TopSideCardsParent;
    public GameObject BottomSideCardsParent;
    public GameObject IsolatedParent;
  //  [HideInInspector]
    public GameObject SelectedCard;
   // [HideInInspector]
    public GameObject HoveredCard;
    //[HideInInspector]
    public List<SpeciesCard> FormerCards;
   // [HideInInspector]
    public List<SpeciesCard> LatterCards;
    GameObject activeLine;
    List<GameObject> isolatedPics = new List<GameObject>();
    List<GameObject> isolatedText = new List<GameObject>();
    bool locked = false;

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();
        foreach (Transform child in TopSideCardsParent.transform)
        {
            if (child.GetComponent<SpeciesCard>())
                FormerCards.Add(child.GetComponent<SpeciesCard>());
        }
        foreach (Transform child in BottomSideCardsParent.transform)
        {
            if (child.GetComponent<SpeciesCard>())
                LatterCards.Add(child.GetComponent<SpeciesCard>());
        }
    }
    bool inLine(GameObject comparison)
    {
        if (SelectedCard)
        {
            if (FormerCards.Exists(c => c.Equals(SelectedCard.GetComponent<SpeciesCard>())) && FormerCards.Exists(c => c.Equals(comparison.GetComponent<SpeciesCard>())))
                return true;

            else if (LatterCards.Exists(c => c.Equals(SelectedCard.GetComponent<SpeciesCard>())) && LatterCards.Exists(c => c.Equals(comparison.GetComponent<SpeciesCard>())))
                return true;
        }
        return false;
    }

    public void Shuffle()
    {
        TopSideCardsParent.GetComponent<ChildShuffler>().ShuffleChildren();
        BottomSideCardsParent.GetComponent<ChildShuffler>().ShuffleChildren();
    }

    public void OnClickPrompt(GameObject caller)
    {
        locked = false;
        if (GameState.instance._currentState != GameState.GameStatus.InGame)
            return;

        if (caller.Equals(SelectedCard))
        {
            caller.GetComponent<Animator>().SetBool("Selected", false);
            Destroy(activeLine);
            SelectedCard = null;
        }
        if (!activeLine)
        {
            // caller.GetComponent<Animator>().SetBool("Selected", true);
            SelectedCard = caller;
            SpawnLine(caller.transform.position);
        }
    }


    public void TemporaryIsolation()
    {
        isolatedText.ForEach(o => o.transform.SetParent(BottomSideCardsParent.transform));
        isolatedPics.ForEach(o => o.transform.SetParent(TopSideCardsParent.transform));
        isolatedText.Clear();
        isolatedPics.Clear();
        isolatedText.TrimExcess();
        isolatedPics.TrimExcess();
        if (LatterCards.Exists(c => c.Equals(HoveredCard.GetComponent<SpeciesCard>())))
        {
            isolatedPics.Add(SelectedCard);
            isolatedText.Add(HoveredCard);
        }
        else
        {
            isolatedPics.Add(HoveredCard);
            isolatedText.Add(SelectedCard);
        }
        isolatedText.ForEach(o => o.transform.SetParent(IsolatedParent.transform));
        isolatedPics.ForEach(o => o.transform.SetParent(IsolatedParent.transform));
       
    }

    public void CorrectAnswerCleanup(GameObject evaluatedCard)
    {
        isolatedText.ForEach(o => o.transform.SetParent(BottomSideCardsParent.transform));
        isolatedPics.ForEach(o => o.transform.SetParent(TopSideCardsParent.transform));
        isolatedText.Clear();
        isolatedPics.Clear();
        isolatedText.TrimExcess();
        isolatedPics.TrimExcess();
        if (LatterCards.Exists(c => c.Equals(evaluatedCard.GetComponent<SpeciesCard>())))
        {
            Debug.Log("if");
            // FormerCards.RemoveAt(FormerCards.FindIndex(c => c.Equals(evaluatedCard.GetComponent<SpeciesCard>())));
            LatterCards.RemoveAt(LatterCards.FindIndex(c => c.Equals(evaluatedCard.GetComponent<SpeciesCard>())));
            Destroy(evaluatedCard);
        }
        else
        {
            Debug.Log("else");
            //  FormerCards.RemoveAt(FormerCards.FindIndex(c => c.Equals(evaluatedCard.GetComponent<SpeciesCard>().CorrectPartner.GetComponent<SpeciesCard>())));
            LatterCards.RemoveAt(LatterCards.FindIndex(c => c.Equals(evaluatedCard.GetComponent<SpeciesCard>().CorrectPartner.GetComponent<SpeciesCard>())));
            Destroy(evaluatedCard.GetComponent<SpeciesCard>().CorrectPartner);
        }
         FormerCards.TrimExcess();
       // LatterCards.TrimExcess();
        
        Destroy(activeLine);
    }

    public void MaintainCurrentLine()
    {
        activeLine.GetComponent<LineFollow>().UpdatePosition = false;
        activeLine = null;
    }

    public void SpawnLine(Vector3 position)
    {
        if (activeLine)
            Destroy(activeLine);
        GameObject temp = Instantiate(LineConnector, position, Quaternion.identity);
        activeLine = temp;
    }

    public void SnapToCardPosition()
    {
        if (GameState.instance._currentState != GameState.GameStatus.InGame)
            return;

        if (!inLine(HoveredCard))
            HoveredCard.GetComponent<Animator>().SetBool("Selected", true);

        if (activeLine && !inLine(HoveredCard))
        {
            LineRenderer lineRenderer = activeLine.GetComponent<LineRenderer>();
            LineFollow lineFollow = activeLine.GetComponent<LineFollow>();
            lineFollow.UpdatePosition = false;
            if (HoveredCard && SelectedCard)
            {
                lineRenderer.positionCount = 4;
                lineRenderer.SetPosition(1, new Vector3(SelectedCard.transform.position.x, FormerCards.Exists(c => c.Equals(SelectedCard.GetComponent<SpeciesCard>())) ? SelectedCard.transform.position.y + 1 : SelectedCard.transform.position.y - 1, SelectedCard.transform.position.z));
                lineRenderer.SetPosition(2, new Vector3(HoveredCard.transform.position.x, LatterCards.Exists(c => c.Equals(HoveredCard.GetComponent<SpeciesCard>())) ? HoveredCard.transform.position.y - 1 : HoveredCard.transform.position.y + 1, HoveredCard.transform.position.z));
                lineRenderer.SetPosition(3, HoveredCard.transform.position);
            }
        }
    }

    public void OnPointerExit()
    {
        if (SelectedCard ? !SelectedCard.Equals(HoveredCard) : true)
            HoveredCard.GetComponent<Animator>().SetBool("Selected", false);
        if (activeLine)
        {
            activeLine.GetComponent<LineRenderer>().positionCount = 3;
            activeLine.GetComponent<LineFollow>().UpdatePosition = true;
        }
            HoveredCard = null;
    }

    public void voidOnMouseUP(GameObject caller)
    {
        if (HoveredCard)
        {
            if (!inLine(HoveredCard))
                HoveredCard.GetComponent<SpeciesCard>().Evaluate();
        }
        Destroy(activeLine);
        if (SelectedCard)
            SelectedCard.GetComponent<Animator>().SetBool("Selected", false);
        SelectedCard = null;
    }

}
