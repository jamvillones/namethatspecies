﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_ResponseOpener : MonoBehaviour {
    public UI_AnimalInfo_Trigger[] Correct;
    public UI_AnimalInfo_Trigger[] Wrong;

    private void OnEnable()
    {
        this.AddEventListenerGlobal<Event_SelectResult>(Event_SelectResult_CB);
    }
    private void OnDisable()
    {
        this.RemoveEventListenerGlobal<Event_SelectResult>(Event_SelectResult_CB);
    }

    void Event_SelectResult_CB(Event_SelectResult e)
    {
        if (e.Correct)
        {
            foreach (UI_AnimalInfo_Trigger a in Correct)
                a.OpenCB();
        }
        else
        {
            foreach (UI_AnimalInfo_Trigger a in Wrong)
                a.OpenCB();
        }
    }
}
