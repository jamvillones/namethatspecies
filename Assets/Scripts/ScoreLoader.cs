﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreLoader : MonoBehaviour {
    Text txt;

	// Use this for initialization
	void Start () {
        txt = GetComponent<Text>();
        txt.text ="Your Score: "+ PlayerPrefs.GetInt("score").ToString();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
