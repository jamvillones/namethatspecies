﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineFollow : MonoBehaviour
{
    public LineRenderer Line;
    public bool UpdatePosition = true;

    // Use this for initialization
    void Start()
    {
        Line = GetComponent<LineRenderer>();
        Color randomColor = new Color(Random.Range(0, 1.0f), Random.Range(0, 1.0f), Random.Range(0, 1.0f));
        Line.startColor = randomColor;
        Line.endColor = randomColor;
        Line.positionCount = 3;
        Line.SetPosition(0, new Vector3(transform.position.x, transform.position.y, transform.position.z - 1));//xy z-1
        Line.SetPosition(1, new Vector3(transform.position.x, transform.position.y, transform.position.z - 1));
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePosition.z = 50;
        Line.SetPosition(2, mousePosition);
    }

    // Update is called once per frame
    void Update()
    {
        if (UpdatePosition)
        {
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePosition.z = 50;
            Line.SetPosition(2, mousePosition);
            transform.LookAt(transform.position + Camera.main.transform.rotation * Vector3.forward, Camera.main.transform.rotation * Vector3.up);
        }
    }
}
