﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Utilities;
using System.Linq;

public class AnimalInfoHolder : Singleton<AnimalInfoHolder>
{
    public string FileName = "/AnimalInfo.dat";
    public List<AnimalInfo> AnimalInfos;
    public List<Texture2D> sprites;


    public void Save()
    {
        this.SaveArray<AnimalInfo>(AnimalInfos.ToArray(), FileName);
        DataChanged dc = new DataChanged();
        dc.type = DataChanged.ChangeType.Saved;
        this.RaiseEventGlobal<DataChanged>(dc);
    }
    public void Load()
    {
        AnimalInfos = this.LoadArray<AnimalInfo>(FileName).ToList();
        DataChanged dc = new DataChanged();
        dc.type = DataChanged.ChangeType.Loaded;
        this.RaiseEventGlobal<DataChanged>(dc);
    }
    private void OnEnable()
    {
        Load();
        foreach(AnimalInfo a in AnimalInfos)
        {
            sprites.Add(Resources.Load(a.Name) as Texture2D);
        }
    }
    private void OnDisable()
    {
        Save();
    }
}
public class DataChanged : GameEvent
{
    public enum ChangeType { Saved, Loaded }
    public ChangeType type;
}

[System.Serializable]
public class AnimalInfo
{
    public string Name;
    public string Description;
    public string PartialDescription;
}

