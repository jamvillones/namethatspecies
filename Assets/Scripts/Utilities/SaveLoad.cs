﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveLoad
{

    public static void Save<T>(this MonoBehaviour m, T e, string fileName)
    {

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.dataPath + fileName);
        bf.Serialize(file, e);
        file.Close();
    }
    public static T Load<T>(this MonoBehaviour m, string fileName) 
    {
        if (!File.Exists(Application.dataPath + fileName))
            return default(T);

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.dataPath + fileName, FileMode.Open);
        T t = (T)bf.Deserialize(file);
        file.Close();
        return t;
    }
    public static void SaveArray<T>(this MonoBehaviour m, T[] data, string fileName)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.dataPath + fileName);
        DataWrapper<T> wrapper = new DataWrapper<T>();
        wrapper.dataList = data;
        bf.Serialize(file, wrapper);
        file.Close();
    }
    public static T[] LoadArray<T>(this MonoBehaviour m,string fileName)
    {
        if (!File.Exists(Application.dataPath + fileName))
            return new T[0];

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.dataPath + fileName, FileMode.Open);
        DataWrapper<T> wrapper = new DataWrapper<T>();
        wrapper = (DataWrapper<T>)bf.Deserialize(file);
        file.Close();
        return wrapper.dataList;
    }
}

[System.Serializable]
public class DataWrapper<T>
{

    public T[] dataList;
}
