﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Holder")]
public class DataHolder : ScriptableObject {
    public List<Species> list;
}
[System.Serializable]
public class Species
{
    public string Name;
    public string ScientificName;
    public string Description;
    public Texture2D Image;
    public Species() { }
}

