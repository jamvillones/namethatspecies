﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoTriggers : MonoBehaviour {

    public void Disable()
    {
        Destroy(gameObject);
       // gameObject.SetActive(false);
    }
    void GameStatChanged_CB(Event_GameStatusChanged e)
    {
        GetComponent<UnityEngine.UI.Text>().enabled = true;
        GetComponent<Animator>().enabled = true;
    }
    private void OnEnable()
    {
        this.AddEventListenerGlobal<Event_GameStatusChanged>(GameStatChanged_CB);
    }
    private void OnDisable()
    {
        this.RemoveEventListenerGlobal<Event_GameStatusChanged>(GameStatChanged_CB);
    }
}
