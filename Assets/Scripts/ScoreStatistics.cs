﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreStatistics : Core.Utilities.Singleton<ScoreStatistics> {
    public int Score;
    public int NumTries;
}
