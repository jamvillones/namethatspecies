﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrobTrigger : MonoBehaviour
{
    public ScoreChanged.ScoreType type;
    public void OnEnable()
    {
        this.AddEventListenerGlobal<ScoreChanged>(scoreChanged_CB);
    }
    public void OnDisable()
    {
        this.RemoveEventListenerGlobal<ScoreChanged>(scoreChanged_CB);
    }
    void scoreChanged_CB(ScoreChanged s)
    {
        if (type == ScoreChanged.ScoreType.scoreOnly && s.type == ScoreChanged.ScoreType.scoreOnly)
            return;
        GetComponent<Animator>().SetTrigger("Throb");
    }
}
    
