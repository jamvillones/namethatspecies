﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeciesCard : MonoBehaviour {
    //public CardType CType;
    public GameObject CorrectPartner;
    [SerializeField]
    Species Mspecies;

    public Species species { get { return Mspecies; } }

    public void AssignSpecies(Species s)
    {
        Mspecies = s;
    }
    public void Evaluate()
    {
        if (SelectionManager.instance.SelectedCard)
        {
            //Manager.MaintainCurrentLine();

            if (SelectionManager.instance.SelectedCard.Equals(CorrectPartner))
            {
                Event_SelectResult e = new Event_SelectResult(true, Mspecies);
                this.RaiseEventGlobal<Event_SelectResult>(e);

                GameState.instance._currentState = GameState.GameStatus.PanelShow;

                SelectionManager.instance.CorrectAnswerCleanup(gameObject);
                SelectionManager.instance.Shuffle();
            }
            else
            {
                GameState.instance._currentState = GameState.GameStatus.PanelShow;

                Event_SelectResult e = new Event_SelectResult(false, SelectionManager.instance.SelectedCard.GetComponent<SpeciesCard>().Mspecies);
                this.RaiseEventGlobal<Event_SelectResult>(e);

                SelectionManager.instance.TemporaryIsolation();
                //SelectionManager.instance.ShuffleAnimals();
            }



        }
    }

    public void MouseUp()
    {
        SelectionManager.instance.voidOnMouseUP(gameObject);
    }

    public void InitializeClicked()
    {

        SelectionManager.instance.OnClickPrompt(gameObject);
    }

    public void OnHover()
    {
        SelectionManager.instance.HoveredCard = gameObject;
        SelectionManager.instance.SnapToCardPosition();
    }

    public void OnExit()
    {
        SelectionManager.instance.OnPointerExit();
    }
}
public enum CardType { SName, Image}

