﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeciesAssign : MonoBehaviour {
    public DataHolder Holder;
    SpeciesCard[] cards;

    private void Awake()
    {
        cards = GetComponentsInChildren<SpeciesCard>();
        for (int i = 0; i < cards.Length; i++)
        {
            cards[i].AssignSpecies(Holder.list[i]);
        }
        Destroy(this);
    }
}
