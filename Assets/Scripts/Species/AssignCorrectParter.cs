﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssignCorrectParter : MonoBehaviour {
    public Transform Illustration, ScientificName;
    private void Awake()
    {
        SpeciesCard[] ICards = Illustration.GetComponentsInChildren<SpeciesCard>();
        SpeciesCard[] Scards = ScientificName.GetComponentsInChildren<SpeciesCard>();
        for(int i =0; i < ICards.Length;i++)
        {
            ICards[i].CorrectPartner = Scards[i].gameObject;
            Scards[i].CorrectPartner = ICards[i].gameObject;
        }
        Destroy(this, 1);
    }
}
