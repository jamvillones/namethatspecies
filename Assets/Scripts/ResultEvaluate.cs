﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultEvaluate : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnEnable()
    {
        //this.AddEventListenerGlobal<GameResult>(GameResultCB);
    }
    private void OnDisable()
    {
       // this.RemoveEventListenerGlobal<GameResult>(GameResultCB);
    }
    public void GameResultCB()
    {
        GameState.instance._currentState = GameState.GameStatus.InGame;
        if (ScoreStatistics.instance.NumTries <= 0)
        {
            PlayerPrefs.SetInt("score",ScoreStatistics.instance.Score);

            if (ScoreStatistics.instance.Score >= 120)
            {
                ApplicationEssentials.instance.LoadScene("PerfectScore");
            }
            else if (ScoreStatistics.instance.Score > 0 && ScoreStatistics.instance.Score < 120)
                ApplicationEssentials.instance.LoadScene("75percentScore");
            else if (ScoreStatistics.instance.Score <= 0)
            {
                ApplicationEssentials.instance.LoadScene("zeroScore");
            }
        }   
    }
}
