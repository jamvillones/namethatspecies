﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour
{

    public GameObject CorrectPartner;
    public AnimalInfo info;
    private void Start()
    {
        if (AnimalInfoHolder.instance.AnimalInfos.Exists(a => a.Name == info.Name))
            info = AnimalInfoHolder.instance.AnimalInfos.Find(a => a.Name == info.Name);
    }

    public void Evaluate()
    {
        if (SelectionManager.instance.SelectedCard)
        {
            //Manager.MaintainCurrentLine();

            if (SelectionManager.instance.SelectedCard.Equals(CorrectPartner))
            {
                //Event_SelectResult e = new Event_SelectResult(true, info);
                //this.RaiseEventGlobal<Event_SelectResult>(e);
                //GameState.instance._currentState = GameState.GameStatus.PanelShow;
                //SelectionManager.instance.CorrectAnswerCleanup(gameObject);
                //SelectionManager.instance.ShuffleAnimals();
            }
            else
            {
                //GameState.instance._currentState = GameState.GameStatus.PanelShow;
                //Event_SelectResult e = new Event_SelectResult(false, SelectionManager.instance.SelectedCard.GetComponent<Card>().info);
                //this.RaiseEventGlobal<Event_SelectResult>(e);
                //SelectionManager.instance.TemporaryIsolation();
                ////SelectionManager.instance.ShuffleAnimals();
            }
                

            
        }
    }

    public void MouseUp()
    {
        SelectionManager.instance.voidOnMouseUP(gameObject);
    }

    public void InitializeClicked()
    {
       
        SelectionManager.instance.OnClickPrompt(gameObject);
    }

    public void OnHover()
    {
        SelectionManager.instance.HoveredCard = gameObject;
        SelectionManager.instance.SnapToCardPosition();
    }

    public void OnExit()
    {
        SelectionManager.instance.OnPointerExit();
    }
}
public class Event_SelectResult: GameEvent
{
    public bool Correct;
    public Species animalinfo;
    public Event_SelectResult()
    {

    }
    public Event_SelectResult(bool b, Species animal)
    {
        Correct = b;
        animalinfo = animal;
    }
}
